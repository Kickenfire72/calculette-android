package com.example.myapp_calc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.objecthunter.exp4j.*;

public class MainActivity extends AppCompatActivity {
    public final String baseExpression = "_";
    public final String baseResult = "Result = ";
    public String expression = "";

    private void makeToast(String text) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast.makeText(context, text, duration).show();
    }

    public void update(View v) {
        TextView screen = findViewById(R.id.screen);
        Button b = (Button) v;
        expression += b.getText().toString();
        screen.setText(expression + baseExpression);
    }

    public void clear(View v) {
        TextView screen = findViewById(R.id.screen);
        expression = "";
        screen.setText(baseExpression);
    }

    public void clearEntry(View v) {
        TextView screen = findViewById(R.id.screen);
        if (!expression.isEmpty()) {
            expression = expression.substring(0, expression.length() - 1);
            screen.setText(expression + baseExpression);
        }
        else {
            screen.setText(baseExpression);
        }
    }

    public void calculate(View v) {
        TextView screen = findViewById(R.id.screen);
        TextView result = findViewById(R.id.resultScreen);
        try {
            if (!expression.isEmpty()) {
                Expression e = new ExpressionBuilder(expression).build();
                double value = e.evaluate();
                result.setText(baseResult + String.valueOf(value));
                screen.setText(baseExpression);
                expression = "";
            } else {
                screen.setText(baseExpression);
            }
        } catch (ArithmeticException e) {
            makeToast("ERROR : DIVISION BY ZERO");
        } catch (IllegalArgumentException e) {
            makeToast("ERROR : BAD SYNTAX");
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


}